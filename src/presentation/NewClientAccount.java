package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import bank.Account;
import bank.Bank;
import bank.Person;
import bank.SavingAccount;
import bank.SpendingAccount;

public class NewClientAccount implements ActionListener{
	Bank bank;
	Person client;
	JFrame appFrame;
	Container cPane;
	JLabel jlbName, jlbEmail, jlbAddress, jlbPhone, jlbInitialBalance, jlbType;
	JTextField jtfName, jtfEmail, jtfAddress, jtfPhone, jtfInitialBanalnce;
	JPanel personPanel, accountPanel;
	JButton jbCreate;
	JComboBox jcboType;
	AdminView adminView;
	
	public NewClientAccount(Bank bank, AdminView adminView) {
		this.bank = bank;
		this.adminView = adminView;
		prepareGUI();
	}
	
	private void prepareGUI() {
		appFrame = new JFrame("NewBankAccount");
		appFrame.setSize(900, 700);
		appFrame.setLocation(500, 200);
		appFrame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				 if (JOptionPane.showConfirmDialog(appFrame, 
				            "Are you sure to close this window?", "Really Closing?", 
				            JOptionPane.YES_NO_OPTION,
				            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					 adminView.update();
					 appFrame.setVisible(false);
					 System.gc();
				 }
			}
		});
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	private void arrangeComponents() {
		personPanel = new JPanel();
		personPanel.setBounds(20, 20, 700, 180);
		personPanel.setBorder(new TitledBorder(new EtchedBorder(), "New Person Information"));
		personPanel.setLayout(null);
		
		jlbName = new JLabel("name");
		jlbName.setBounds(50, 50, 110, 30);
		jlbName.setFont(jlbName.getFont().deriveFont(16f));
		personPanel.add(jlbName);
		
		jlbPhone = new JLabel("phoneNumber");
		jlbPhone.setBounds(200, 50, 110, 30);
		jlbPhone.setFont(jlbPhone.getFont().deriveFont(16f));
		personPanel.add(jlbPhone);
		
		jlbAddress = new JLabel("address");
		jlbAddress.setBounds(375, 50, 110, 30);
		jlbAddress.setFont(jlbAddress.getFont().deriveFont(16f));
		personPanel.add(jlbAddress);
		
		jlbEmail = new JLabel("email");
		jlbEmail.setBounds(540, 50, 110, 30);
		jlbEmail.setFont(jlbEmail.getFont().deriveFont(16f));
		personPanel.add(jlbEmail);
		
		jtfName = new JTextField("");
		jtfName.setBounds(10, 90, 140, 30);
		jtfName.setFont(jtfName.getFont().deriveFont(16f));;
		personPanel.add(jtfName);
		
		jtfPhone = new JTextField("");
		jtfPhone.setBounds(175, 90, 150, 30);
		jtfPhone.setFont(jtfPhone.getFont().deriveFont(16f));;
		personPanel.add(jtfPhone);
		
		jtfAddress = new JTextField("");
		jtfAddress.setBounds(340, 90, 140, 30);
		jtfAddress.setFont(jtfAddress.getFont().deriveFont(16f));;
		personPanel.add(jtfAddress);
		
		jtfEmail = new JTextField("");
		jtfEmail.setBounds(495, 90, 140, 30);
		jtfEmail.setFont(jtfEmail.getFont().deriveFont(16f));;
		personPanel.add(jtfEmail);
		
		cPane.add(personPanel);
		
		accountPanel = new JPanel();
		accountPanel.setBounds(20, 300, 700, 180);
		accountPanel.setBorder(new TitledBorder(new EtchedBorder(), "New Account Information"));
		accountPanel.setLayout(null);
		
		jlbInitialBalance = new JLabel("InitialBalance");
		jlbInitialBalance.setBounds(50, 330, 130, 30);
		jlbInitialBalance.setFont(jlbInitialBalance.getFont().deriveFont(16f));
		cPane.add(jlbInitialBalance);
		
		jtfInitialBanalnce = new JTextField("");
		jtfInitialBanalnce.setBounds(40, 370, 140, 30);
		jtfInitialBanalnce.setFont(jtfInitialBanalnce.getFont().deriveFont(16f));;
		cPane.add(jtfInitialBanalnce);
		
		jlbType = new JLabel("AccountType");
		jlbType.setBounds(200, 330, 200, 30);
		jlbType.setFont(jlbType.getFont().deriveFont(16f));
		cPane.add(jlbType);
		
		jcboType = new JComboBox(new String[]{"SpendingAccount", "SavingAccount"});
		jcboType.setBounds(200, 370, 200, 30);
		jcboType.setFont(jcboType.getFont().deriveFont(16f));
		jcboType.setSelectedIndex(0);
		cPane.add(jcboType);
		
		cPane.add(accountPanel);
		
		jbCreate = new JButton("Create");
		jbCreate.setBounds(420, 600, 90, 40);
		jbCreate.setFont(jbCreate.getFont().deriveFont(16f));
		jbCreate.addActionListener(this);
		cPane.add(jbCreate);
	}
	
	public void actionPerformed(ActionEvent e) {
first :	if (e.getSource() == jbCreate) {
			Person person = new Person(bank.getCurrentIdPerson(), jtfName.getText(), jtfPhone.getText(), jtfAddress.getText(), jtfEmail.getText());
			 try {
				bank.addPerson(person);
			} catch (IllegalArgumentException i) {
				JOptionPane.showMessageDialog(appFrame, i.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			} catch (Exception a) {
				JOptionPane.showMessageDialog(appFrame, a.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			}
			Account account;
			if (jcboType.getSelectedIndex() == 0) {
				if (jtfInitialBanalnce.getText().equals(""))
					account = new SpendingAccount(bank.getCurrentIdAccount(), bank.getAddedPerson());
				else
					account = new SpendingAccount(bank.getCurrentIdAccount(), Integer.parseInt(jtfInitialBanalnce.getText()),bank.getAddedPerson());
			} else {
				if (jtfInitialBanalnce.getText().equals(""))
					account = null;
				else {
					JDialog.setDefaultLookAndFeelDecorated(true);
					Object selection = JOptionPane.showInputDialog(null, "Please insert the interestRate! (%)", JOptionPane.QUESTION_MESSAGE);
					account = new SavingAccount(bank.getCurrentIdAccount(), Integer.parseInt(jtfInitialBanalnce.getText()),bank.getAddedPerson(), Double.parseDouble((String)selection));
				}
			}
			try {
				bank.addAccount(account);
			} catch (IllegalArgumentException i) {
				JOptionPane.showMessageDialog(appFrame, i.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			} /*catch (Exception a) {
				JOptionPane.showMessageDialog(appFrame, a.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			}*/
			JOptionPane.showMessageDialog(appFrame, account.toString() + " | holder: " + account.getAccountHolder().toString() + " succesfully created!", "Success!", JOptionPane.INFORMATION_MESSAGE);
			jtfName.setText(""); jtfPhone.setText(""); jtfAddress.setText(""); jtfEmail.setText(""); jtfInitialBanalnce.setText(""); jcboType.setSelectedIndex(0);
		}
	}
}
