package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import bank.Bank;
import bank.Person;

public class Meniu implements ActionListener{
	
	JFrame appFrame;
	Container cPane;
	JButton jbClient, jbAdmin;
	ClientView cView;
	AdminView aView;
	Bank bank;
	
	public Meniu(Bank bank) {
		this.bank = bank;
		prepareGUI();
	}
	
	private void prepareGUI() {
		appFrame = new JFrame("Meniu");
		appFrame.setSize(300, 200);
		appFrame.setLocation(700, 300);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	private void arrangeComponents() {
		// button pentru accesarea drepturilor de client
		jbClient = new JButton("Client");
		jbClient.setBounds(35, 75, 100, 50);
		jbClient.setFont(jbClient.getFont().deriveFont(15f));
		jbClient.addActionListener(this);
		cPane.add(jbClient);
		
		// button pentru accesarea drepturilor de admin
		jbAdmin = new JButton("Admin");
		jbAdmin.setBounds(160, 75, 100, 50);
		jbAdmin.setFont(jbAdmin.getFont().deriveFont(15f));
		jbAdmin.addActionListener(this);
		cPane.add(jbAdmin);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbClient) {
			JDialog.setDefaultLookAndFeelDecorated(true);
			Object[] selectionValues = new Object[bank.getPersons().size()];
			int i = 0;
			for (Person client : bank.getPersons()) {
				selectionValues[i++] = client.getIdPerson();
			}
			if (i >= 1) {
				int initialSelection = (int)selectionValues[0];
				Object selection = JOptionPane.showInputDialog(null, "Please select your bank Account based on your id!", "Bank Account Access", JOptionPane.QUESTION_MESSAGE, null, selectionValues, initialSelection);
				if (selection != null) {
					Person client = bank.getPersonById((int)selection);
					cView = new ClientView(this.bank, client);
				}
				appFrame.setVisible(false);
			} else {
				JOptionPane.showMessageDialog(appFrame, "No clients avabile!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
			}
		} else if (e.getSource() == jbAdmin) {
			aView = new AdminView(this.bank);
			appFrame.setVisible(false);
		}
	}
}
