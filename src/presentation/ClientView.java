package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import bank.Account;
import bank.Bank;
import bank.Person;
import bank.SpendingAccount;

public class ClientView implements ActionListener{
	
	Bank bank;
	Person client;
	JFrame appFrame;
	Container cPane;
	JButton jbWithdraw, jbDeposit, jbFindById;
	JTextField jtfFindById;
	JScrollPane js;
	JTable jTable;
	Object[][] line;
	List<Object> values = new ArrayList<Object>();
	
	
	public ClientView(Bank bank, Person client) {
		this.bank = bank;
		this.client = client;
		prepareGUI();
		update();
	}
	
	private void update() {
		if (js != null)
			cPane.remove(js);
		if (jTable != null)
			cPane.remove(jTable);
		if (!values.isEmpty())
			values.clear();
		cPane.revalidate();
		js = new JScrollPane();
		js.setBounds(50, 50, 700, 300);
		line = new Object[bank.getPersonAccounts(client).size()][4];
		int i = 0, j;
		for (Account account : bank.getPersonAccounts(client)) {
			j = 0;
			values.add(account.getAccountId());
			values.add(account.getBalance());
			values.add(account.getTransactionCount());
			values.add(account.getClass().getName());
			for (Object obj : values)
				line[i][j++] = obj;
			values.clear();
			i++;
		}
		String[] columns = {"AccountId", "Balance", "TransactionCount", "AccountType"};
		jTable = new JTable(line, columns);
		js.setViewportView(jTable);
		cPane.add(js);
	}
	
	private void prepareGUI() {
		appFrame = new JFrame("BankAccount");
		appFrame.setSize(950, 600);
		appFrame.setLocation(500, 200);
		appFrame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				 if (JOptionPane.showConfirmDialog(appFrame, 
				            "Are you sure to close this window?", "Really Closing?", 
				            JOptionPane.YES_NO_OPTION,
				            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					 bank.writeBankAccounts();
					 bank.writeCurrentIds();
					 System.exit(0);
				 }
			}
		});
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	private void arrangeComponents() {
		
		jbWithdraw = new JButton("Withdraw");
		jbWithdraw.setBounds(780, 200, 120, 40);
		jbWithdraw.setFont(jbWithdraw.getFont().deriveFont(16f));
		jbWithdraw.addActionListener(this);
		cPane.add(jbWithdraw);
		
		jbDeposit = new JButton("Deposit");
		jbDeposit.setBounds(780, 310, 120, 40);
		jbDeposit.setFont(jbDeposit.getFont().deriveFont(16f));
		jbDeposit.addActionListener(this);
		cPane.add(jbDeposit);
		
		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(780, 50, 120, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);
		
		jtfFindById = new JTextField("");
		jtfFindById.setBounds(780, 100, 120, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));;
		cPane.add(jtfFindById);
	}
	
	public void actionPerformed(ActionEvent e) {
eticheta:if (e.getSource() == jbWithdraw) {
			int row = -1;
			row = jTable.getSelectedRow();
			if (row == -1) {
				JOptionPane.showMessageDialog(appFrame, "Please select an account first!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
				break eticheta;
			}
			Account account = bank.getAccountById((int) jTable.getValueAt(row, 0), client.getIdPerson());
			if (account.getClass() == SpendingAccount.class) {
				Object selection = 0;
				if (account.free()) {
					JDialog.setDefaultLookAndFeelDecorated(true);
					selection = JOptionPane.showInputDialog(null, "Please insert the amount(max:" + account.getBalance() + " )!", JOptionPane.QUESTION_MESSAGE);
				} else {
					JDialog.setDefaultLookAndFeelDecorated(true);
					selection = JOptionPane.showInputDialog(null, "Please insert the amount(max:" + (account.getBalance() - account.getTransactionFee()) + " )!", JOptionPane.QUESTION_MESSAGE);
				}
				if (Double.parseDouble((String)selection) <= 0)
					JOptionPane.showMessageDialog(appFrame, "Invalid amount!", "Error!", JOptionPane.ERROR_MESSAGE);
				if (account.free()) {
					if (Double.parseDouble((String)selection) > account.getBalance())
						JOptionPane.showMessageDialog(appFrame, "Insuficient funds!", "Error!", JOptionPane.ERROR_MESSAGE);
					else {
						account.withdraw(Double.parseDouble((String)selection));
						JOptionPane.showMessageDialog(appFrame, Double.parseDouble((String)selection)  + " withdrawed!", "Success!", JOptionPane.INFORMATION_MESSAGE);
					}
				} else {
					if (Double.parseDouble((String)selection) > (account.getBalance() -  account.getTransactionFee()))
						JOptionPane.showMessageDialog(appFrame, "Insuficient funds!", "Error!", JOptionPane.ERROR_MESSAGE);
					else {
						account.withdraw(Double.parseDouble((String)selection));
						JOptionPane.showMessageDialog(appFrame, Double.parseDouble((String)selection)  + " withdrawed!", "Success!", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			} else {
				JOptionPane.showMessageDialog(appFrame, "You will be able to withdraw all your balance, no more or less!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
				JDialog.setDefaultLookAndFeelDecorated(true);
				Object selection = JOptionPane.showInputDialog(null, "Please insert the amount(" + account.getBalance() + ")!", JOptionPane.QUESTION_MESSAGE);
				if (Double.parseDouble((String)selection) - account.getBalance() > 0.99)
					JOptionPane.showMessageDialog(appFrame, "Bad withdraw amount, please insert exact sum!", "Error!", JOptionPane.ERROR_MESSAGE);
				else {
					account.withdraw(Double.parseDouble((String)selection));
					JOptionPane.showMessageDialog(appFrame, Double.parseDouble((String)selection)  + " withdrawed!", "Success!", JOptionPane.INFORMATION_MESSAGE);
				}	
			}
			update();	
		} else if (e.getSource() == jbFindById) {
			if (jtfFindById.getText() != "") {
				Account account = bank.getAccountById(Integer.parseInt(jtfFindById.getText()), client.getIdPerson());
				if (account != null) {
					for (int i = 0; i < jTable.getRowCount(); i++)
						if ((int)jTable.getValueAt(i, 0) == account.getAccountId())
							jTable.setRowSelectionInterval(i, i);
				} else {
					JOptionPane.showMessageDialog(appFrame, "Account not found!", "Not found!", JOptionPane.ERROR_MESSAGE);
				}
				jtfFindById.setText("");
			} else {
				JOptionPane.showMessageDialog(appFrame, "Please insert the ID", "Error!", JOptionPane.ERROR_MESSAGE);
			}
		} else if (e.getSource() == jbDeposit) {
			int row = -1;
			row = jTable.getSelectedRow();
			if (row == -1) {
				JOptionPane.showMessageDialog(appFrame, "Please select an account first!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
				break eticheta;
			}
			Account account = bank.getAccountById((int) jTable.getValueAt(row, 0), client.getIdPerson());
			if (account.getClass() == SpendingAccount.class) {
				JDialog.setDefaultLookAndFeelDecorated(true);
				Object selection = JOptionPane.showInputDialog(null, "Please insert the amount!", JOptionPane.QUESTION_MESSAGE);
				if (Double.parseDouble((String)selection) <= 0)
					JOptionPane.showMessageDialog(appFrame, "Invalid amount!", "Error!", JOptionPane.ERROR_MESSAGE);
				else {
					account.deposit(Double.parseDouble((String)selection));
					JOptionPane.showMessageDialog(appFrame, Double.parseDouble((String)selection)  + " deposited!", "Success!", JOptionPane.INFORMATION_MESSAGE);
				}
			} else
				JOptionPane.showMessageDialog(appFrame, "Invalid operation on Saving Accounts!", "Error!", JOptionPane.ERROR_MESSAGE);
			update();
		}
	}
}
