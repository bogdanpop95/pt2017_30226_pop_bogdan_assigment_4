package presentation;

import java.awt.Container;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import bank.Bank;
import bank.Person;

public class AdminView implements ActionListener, MouseListener{

	Bank bank;
	JFrame appFrame;
	Container cPane;
	JButton jbCreate, jbEdit, jbDelete, jbFindById;
	JTextField jtfFindById;
	JScrollPane js;
	JTable jTable;
	Object[][] line;
	List<Object> values = new ArrayList<Object>();
	Integer timerinterval = (Integer) Toolkit.getDefaultToolkit().getDesktopProperty("awt.multiClickInterval");
	Timer timer = new Timer(timerinterval + 1, this);
	boolean flag = false;

	public AdminView(Bank bank) {
		this.bank = bank;
		prepareGUI();
		update();
	}

	public void update() {
		if (js != null)
			cPane.remove(js);
		if (jTable != null)
			cPane.remove(jTable);
		if (!values.isEmpty())
			values.clear();
		cPane.revalidate();
		js = new JScrollPane();
		js.setBounds(50, 50, 800, 400);
		line = new Object[bank.getPersons().size()][5];
		int i = 0, j;
		for (Person client : this.bank.getPersons()) {
			j = 0;
			values.add(client.getIdPerson());
			values.add(client.getName());
			values.add(client.getPhoneNumber());
			values.add(client.getAddress());
			values.add(client.getEmail());
			for (Object obj : values)
				line[i][j++] = obj;
			values.clear();
			i++;
		}
		String[] columns = { "PersonId", "Name", "PhoneNumber", "Address", "Email" };
		jTable = new JTable(line, columns);
		js.setViewportView(jTable);
		cPane.add(js);
		js.addMouseListener(this);
	}

	private void prepareGUI() {
		appFrame = new JFrame("BankView");
		appFrame.setSize(1100, 700);
		appFrame.setLocation(300, 20);
		appFrame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(appFrame, "Are you sure to close this window?", "Really Closing?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					bank.writeBankAccounts();
					bank.writeCurrentIds();
					System.exit(0);
				}
			}
		});
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);

		arrangeComponents();
		appFrame.setVisible(true);
	}

	private void arrangeComponents() {
		jbCreate = new JButton("Create bank account");
		jbCreate.setBounds(40, 500, 300, 40);
		jbCreate.setFont(jbCreate.getFont().deriveFont(16f));
		jbCreate.addActionListener(this);
		cPane.add(jbCreate);

		jbEdit = new JButton("Edit");
		jbEdit.setBounds(950, 250, 90, 40);
		jbEdit.setFont(jbEdit.getFont().deriveFont(16f));
		jbEdit.addActionListener(this);
		cPane.add(jbEdit);

		jbDelete = new JButton("Delete");
		jbDelete.setBounds(950, 410, 90, 40);
		jbDelete.setFont(jbDelete.getFont().deriveFont(16f));
		jbDelete.addActionListener(this);
		cPane.add(jbDelete);

		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(950, 50, 90, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);

		jtfFindById = new JTextField("");
		jtfFindById.setBounds(950, 100, 90, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));
		cPane.add(jtfFindById);
	}

	public void actionPerformed(ActionEvent e) {
eticheta: if (e.getSource() == jbCreate) {
			NewClientAccount newClientAccount = new NewClientAccount(this.bank, this);
			//update();
		} else if (e.getSource() == jbEdit) {
			int row = -1;
			row = jTable.getSelectedRow();
			if (row == -1) {
				JOptionPane.showMessageDialog(appFrame, "Please select a person first!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
				break eticheta;
			}
			int columns = jTable.getColumnCount();
			String[] valori = new String[columns];
			Person oldPerson = null;
			try {
				oldPerson = bank.getPersonById((int)jTable.getValueAt(row, 0));
			} catch (ClassCastException v) {
				JOptionPane.showMessageDialog(appFrame, "ID edit is invalid!", "Error!", JOptionPane.ERROR_MESSAGE);
				update();
				break eticheta;
			}
			for (int i = 1; i < columns; i++) {
				valori[i] = (String) jTable.getValueAt(row, i);
			}
			Person newPerson = new Person(oldPerson.getIdPerson(), valori[1], valori[2], valori[3], valori[4]);
			try {
				bank.editPerson(oldPerson, newPerson);
			} catch (IllegalArgumentException i) {
				JOptionPane.showMessageDialog(appFrame, i.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
			} catch (Exception a) {
				JOptionPane.showMessageDialog(appFrame, a.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
			}
			update();
			JOptionPane.showMessageDialog(appFrame, newPerson.toString() + " succesfully edited!", "Success!", JOptionPane.INFORMATION_MESSAGE);
		} else if (e.getSource() == jbDelete) {
			int row = -1;
			row = jTable.getSelectedRow();
			if (row == -1) {
				JOptionPane.showMessageDialog(appFrame, "Please select a person first!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
				break eticheta;
			}
			Person person = bank.getPersonById((int) jTable.getValueAt(row, 0));
			bank.removePerson(person);
			update();
			System.gc();
		} else if (e.getSource() == jbFindById) {
			Person person = bank.getPersonById(Integer.parseInt(jtfFindById.getText()));
			if (person != null)
				for (int i = 0; i < jTable.getRowCount(); i++) {
					if ((int) jTable.getValueAt(i, 0) == person.getIdPerson())
						jTable.setRowSelectionInterval(i, i);
				}
			else {
				JOptionPane.showMessageDialog(appFrame,"Persoana cu id: " + Integer.parseInt(jtfFindById.getText()) + " nu exista!" , "INFO!", JOptionPane.INFORMATION_MESSAGE);
				jtfFindById.setText("");
				}
		}
	}

	public void mouseClicked(MouseEvent event) {
		int row;
		Person client;
		if (SwingUtilities.isRightMouseButton(event)) {
			timer.start();
			if (event.getClickCount() == 2) {
				System.out.println("Double clicked!");
				row = jTable.getSelectedRow();
				client = bank.getPersonById((int) jTable.getValueAt(row, 0));
				SeeAccounts clientAccounts = new SeeAccounts(this.bank, client);
				/*if (bank.getPersonAccounts(client).isEmpty()) {
					bank.removePerson(client);
					update();
				}*/
			}
			if (timer.isRunning())
					;
			else
				flag = true;
					
			if (event.getClickCount() == 1 && flag) {
				jTable.clearSelection();
				flag = false;
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
