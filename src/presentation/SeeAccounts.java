package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import bank.Account;
import bank.Bank;
import bank.Person;
import bank.SavingAccount;
import bank.SpendingAccount;

public class SeeAccounts implements ActionListener{
	Bank bank;
	Person client;
	JFrame appFrame;
	Container cPane;
	JButton jbAdd, jbEdit, jbDelete, jbFindById;
	JLabel jlbName, jlbEmail, jlbAddress, jlbPhone, jlbInitialBalance, jlbType;
	JTextField jtfFindById, jtfInitialBanalnce;
	JPanel accountPanel;
	JComboBox jcboType;
	JScrollPane js;
	JTable jTable;
	Object[][] line;
	List<Object> values =  new ArrayList<Object>();
	
	public SeeAccounts(Bank bank, Person client) {
		this.bank = bank;
		this.client = client;
		prepareGUI();
		update();
	}
	
	private void update() {
		if (js != null)
			cPane.remove(js);
		if (jTable != null)
			cPane.remove(jTable);
		if (!values.isEmpty())
			values.clear();
		cPane.revalidate();
		js = new JScrollPane();
		js.setBounds(50, 50, 700, 300);
		line = new Object[bank.getPersonAccounts(client).size()][4];
		int i = 0, j;
		for (Account account : bank.getPersonAccounts(client)) {
			j = 0;
			values.add(account.getAccountId());
			values.add(account.getBalance());
			values.add(account.getTransactionCount());
			values.add(account.getClass().getName());
			for (Object obj : values)
				line[i][j++] = obj;
			values.clear();
			i++;
		}
		String[] columns = {"AccountId", "Balance", "TransactionCount", "AccountType"};
		jTable = new JTable(line, columns);
		js.setViewportView(jTable);
		cPane.add(js);
	}
	
	private void prepareGUI() {
		appFrame = new JFrame("BankAccount");
		appFrame.setSize(900, 650);
		appFrame.setLocation(500, 200);
		appFrame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				 if (JOptionPane.showConfirmDialog(appFrame, 
				            "Are you sure to close this window?", "Really Closing?", 
				            JOptionPane.YES_NO_OPTION,
				            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
					 appFrame.setVisible(false);
					 System.gc();
				 }
			}
		});
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arangeComponents();
		appFrame.setVisible(true);
	}
	
	private void arangeComponents() {
		accountPanel = new JPanel();
		accountPanel.setBounds(50, 350, 700, 180);
		accountPanel.setBorder(new TitledBorder(new EtchedBorder(), "New Account Information"));
		accountPanel.setLayout(null);
		
		jlbInitialBalance = new JLabel("InitialBalance");
		jlbInitialBalance.setBounds(90, 380, 150, 30);
		jlbInitialBalance.setFont(jlbInitialBalance.getFont().deriveFont(16f));
		cPane.add(jlbInitialBalance);
		
		jtfInitialBanalnce = new JTextField("");
		jtfInitialBanalnce.setBounds(80, 420, 140, 30);
		jtfInitialBanalnce.setFont(jtfInitialBanalnce.getFont().deriveFont(16f));;
		cPane.add(jtfInitialBanalnce);
		
		jlbType = new JLabel("AccountType");
		jlbType.setBounds(240, 380, 150, 30);
		jlbType.setFont(jlbType.getFont().deriveFont(16f));
		cPane.add(jlbType);
		
		jcboType = new JComboBox(new String[]{"SpendingAccount", "SavingAccount"});
		jcboType.setBounds(240, 420, 200, 30);
		jcboType.setFont(jcboType.getFont().deriveFont(16f));
		jcboType.setSelectedIndex(0);
		cPane.add(jcboType);
		
		cPane.add(accountPanel);
		
		jbAdd = new JButton("Add");
		jbAdd.setBounds(780, 430, 90, 40);
		jbAdd.setFont(jbAdd.getFont().deriveFont(16f));
		jbAdd.addActionListener(this);
		cPane.add(jbAdd);
		
		jbEdit = new JButton("Edit");
		jbEdit.setBounds(780, 200, 90, 40);
		jbEdit.setFont(jbEdit.getFont().deriveFont(16f));
		jbEdit.addActionListener(this);
		cPane.add(jbEdit);
		
		jbDelete = new JButton("Delete");
		jbDelete.setBounds(780, 310, 90, 40);
		jbDelete.setFont(jbDelete.getFont().deriveFont(16f));
		jbDelete.addActionListener(this);
		cPane.add(jbDelete);
		
		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(780, 50, 90, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);
		
		jtfFindById = new JTextField("");
		jtfFindById.setBounds(780, 100, 90, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));;
		cPane.add(jtfFindById);
	}
	
	public void actionPerformed(ActionEvent e) {
first :	if (e.getSource() == jbEdit) {
			int row = -1;
			row = jTable.getSelectedRow();
			if (row == -1) {
				JOptionPane.showMessageDialog(appFrame, "Please select an account first!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
				break first;
			}
			Object value = jTable.getValueAt(row, 1);
			Account oldAccount = null; 
			try {
				oldAccount = bank.getAccountById((int)jTable.getValueAt(row, 0), client.getIdPerson());
			} catch (ClassCastException v) {
				JOptionPane.showMessageDialog(appFrame, "ID edit is invalid!", "Error!", JOptionPane.ERROR_MESSAGE);
				update();
				break first;
			}
			Account newAccount;
			if (oldAccount.getClass() == SpendingAccount.class)
				newAccount = new SpendingAccount(oldAccount.getAccountId(), Double.parseDouble((String)value), oldAccount.getAccountHolder());
			else
				newAccount = new SavingAccount(oldAccount.getAccountId(), Double.parseDouble((String)value), oldAccount.getAccountHolder(), oldAccount.getInterestRate());
			try {
				bank.editAccount(oldAccount, newAccount);
			} catch (IllegalArgumentException i) {
				JOptionPane.showMessageDialog(appFrame, i.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			} catch (Exception a) {
				JOptionPane.showMessageDialog(appFrame, a.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			}
			update();
			JOptionPane.showMessageDialog(appFrame, newAccount.toString() + " succesfully edited!", "Success!", JOptionPane.INFORMATION_MESSAGE);
		} else if (e.getSource() == jbAdd) {
			bank.setAddedPerson(client);
			Account account;
			if (jcboType.getSelectedIndex() == 0) {
				if (jtfInitialBanalnce.getText().equals(""))
					account = new SpendingAccount(bank.getCurrentIdAccount(), bank.getAddedPerson());
				else
					account = new SpendingAccount(bank.getCurrentIdAccount(), Integer.parseInt(jtfInitialBanalnce.getText()),bank.getAddedPerson());
			} else {
				if (jtfInitialBanalnce.getText().equals(""))
					account = null;
				else {
					JDialog.setDefaultLookAndFeelDecorated(true);
					Object selection = JOptionPane.showInputDialog(null, "Please insert the interestRate! (%)", JOptionPane.QUESTION_MESSAGE);
					account = new SavingAccount(bank.getCurrentIdAccount(), Integer.parseInt(jtfInitialBanalnce.getText()),bank.getAddedPerson(), Double.parseDouble((String)selection));
				}
			}
			try {
				bank.addAccount(account);
			} catch (IllegalArgumentException i) {
				JOptionPane.showMessageDialog(appFrame, i.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			} catch (Exception a) {
				JOptionPane.showMessageDialog(appFrame, a.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				break first;
			}
			JOptionPane.showMessageDialog(appFrame, account.toString() + " | holder: " + account.getAccountHolder().toString() + " succesfully created!", "Success!", JOptionPane.INFORMATION_MESSAGE);
			jtfInitialBanalnce.setText(""); jcboType.setSelectedIndex(0);
			update();
		} else if (e.getSource() == jbDelete) {
			int row = -1;
			row = jTable.getSelectedRow();
			if (row == -1) {
				JOptionPane.showMessageDialog(appFrame, "Please select an account first!", "INFO!", JOptionPane.INFORMATION_MESSAGE);
				break first;
			}
			Account account = bank.getAccountById((int)jTable.getValueAt(row, 0), client.getIdPerson());
			bank.removeAccount(account);
			update();
			System.gc();
		} else if (e.getSource() == jbFindById) {
			if (jtfFindById.getText() != "") {
				Account account = bank.getAccountById(Integer.parseInt(jtfFindById.getText()), client.getIdPerson());
				if (account != null) {
					for (int i = 0; i < jTable.getRowCount(); i++)
						if ((int)jTable.getValueAt(i, 0) == account.getAccountId())
							jTable.setRowSelectionInterval(i, i);
				} else {
					JOptionPane.showMessageDialog(appFrame, "Account not found!", "Not found!", JOptionPane.ERROR_MESSAGE);
				}
				jtfFindById.setText("");
			} else {
				JOptionPane.showMessageDialog(appFrame, "Please insert the ID", "Error!", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
