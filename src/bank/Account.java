package bank;

import observerInterfaces.Observer;
import observerInterfaces.Subject;

public abstract class Account implements java.io.Serializable, Subject{
	
	private static final long serialVersionUID = 1L;
	protected int id;
	protected double balance;
	//protected double transferFee;
	protected int transactionCount;
	protected Person holder;
	
	Observer observer;
	
	public Account(int id, Person person) {
		super();
		this.id = id;
		this.holder = person;
		attach(person);
	}
	
	public Account(int id, double initialBalance, Person person) {
		super();
		this.balance = initialBalance;
		this.id = id;
		this.holder = person;
		attach(person);
	}
	
	public void deposit(double amount) {
		this.balance += amount;
		notifyy(amount, 1);
	}
	
	public boolean withdraw(double amount) {
		boolean result = false;
		if (this.balance >= amount) {
			this.balance -= amount;
			result = true;
		}
		notifyy(amount, 2);
		return result;
	}
	
	public double getBalance() {
		return this.balance;
	}
	
/*	public boolean transfer(double amount, Account other) {
		transferFee = 0.01 * amount;
		boolean result = false;
		if (this.withdraw(transferFee + amount)) {
			other.deposit(amount);
			result = true;
		}
		return result;
	}*/
	
	public int getAccountId() {
		return this.id;
	}
	
	public Person getAccountHolder() {
		return this.holder;
	}
	
/*	public double getTransferFee() {
		return this.transferFee;
	}*/
	
	public int getTransactionCount() {
		return this.transactionCount;
	}
	
	// Subject interface methods
	@Override
	public void attach(Observer o) {
		this.observer = o;
	}
	
	@Override
	public void dettach() {
		this.observer = null;
	}
	
	@Override
	public void notifyy(double amount, int op) {
		observer.update(this, amount, op);
	}

	public double getInterestRate() {
		return 0;
	}

	public boolean free() {
		return false;
	}
	
	public abstract double getTransactionFee();
	
}
