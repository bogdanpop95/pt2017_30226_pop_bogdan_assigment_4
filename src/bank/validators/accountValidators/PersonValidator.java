package bank.validators.accountValidators;

import bank.validators.Validator;
import bank.Account;
import bank.Bank;

public class PersonValidator implements Validator<Account>{
	
	private Bank bank;
	
	public PersonValidator(Bank bank) {
		this.bank = bank;
	}
	
	public void validate(Account t) {
		if (!(t.getAccountHolder().equals(bank.getAddedPerson()))) {
			throw new IllegalArgumentException("Invalid person holder !");
		}
	}
}
