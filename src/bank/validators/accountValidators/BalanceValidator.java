package bank.validators.accountValidators;

import bank.Account;
import bank.validators.Validator;

public class BalanceValidator implements Validator<Account> {
	
	public void validate(Account t) {
		if (t.getBalance() < 0)
			throw new IllegalArgumentException("Invalid balance (it must be positive)!");
	}
}
