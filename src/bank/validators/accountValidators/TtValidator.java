package bank.validators.accountValidators;

import bank.Account;
import bank.validators.Validator;

public class TtValidator implements Validator<Account>{
	
	public void validate(Account t) {
		if ( t.getTransactionCount() != 0)
			throw new IllegalArgumentException("Invalid initial transferFee and transaction count (it must be 0)!");
	}
}
