package bank.validators;

/**
 * @author pop_b
 * Interfata care implementeaza metoda de validare a unui obiect de tipul generic T
 * @param <T> obiectul de tipul T care urmeaza sa fie validat
 */
public interface Validator<T> {
	public void validate(T t);
}
