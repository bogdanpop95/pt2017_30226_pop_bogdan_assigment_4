package bank.validators.personValidators;

import java.util.regex.Pattern;
import bank.Person;
import bank.validators.Validator;

public class phoneNumberValidator implements Validator<Person>{
	
	private static final String PHONE_PATTERN = "^[0-9\\-\\+]{9,15}?";
	
	public void validate(Person t) {
		Pattern pattern = Pattern.compile(PHONE_PATTERN);
		if (!pattern.matcher(t.getPhoneNumber()).matches()) {
			throw new IllegalArgumentException("Invalid phoneNumber!");
		}
	}
}
