package bank.validators.personValidators;

import java.util.regex.Pattern;
import bank.Person;
import bank.validators.Validator;

public class NameValidator implements Validator<Person>{
	
	private static final String NAME_PATTERN = "^([a-zA-Z]{2,}\\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)";
	
	public void validate(Person t) {
		Pattern pattern = Pattern.compile(NAME_PATTERN);
		if (!pattern.matcher(t.getName()).matches()) {
			throw new IllegalArgumentException("Invalid name : Valid Characters include (A-Z) (a-z) (' space -)");
		}
	}
}
