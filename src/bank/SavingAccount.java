package bank;

public class SavingAccount extends Account{
	
	private static final long serialVersionUID = 3L;
	private final int WITHDRAW = 1;
	private double interestRate;
	
	public SavingAccount(int id, double initialBalance, Person person, double interestRate) {
		super(id, (interestRate / 100 + 1) * initialBalance, person);
		this.interestRate = interestRate;
	}
	
	@Override
	public void deposit(double amount) {
		assert false : "Actiunea de deposit in SavingAccount este indisponibila!";
	}
	
	@Override
	public boolean withdraw(double amount) {
		if (this.transactionCount < WITHDRAW) {
			if(super.withdraw(amount)) {
				this.transactionCount++;
				return true;
			}
		}
		return false;
	}
	
/*	public boolean transfer(double amount, Account other) {
		if (this.transactionCount < WITHDRAW) {
			if (super.transfer(amount, other)) {
				this.transactionCount++;
				return true;
			}
		}
		return false;
	}*/
	
	public String toString() {
		return "SavingAccount" + "| id: " + this.getAccountId();
	}
	
	public double getInterestRate() {
		return this.interestRate;
	}
	
	public double getTransactionFee() {
		return 0;
	}
}
