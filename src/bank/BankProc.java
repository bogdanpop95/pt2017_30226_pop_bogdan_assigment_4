package bank;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public interface BankProc {
	/**
	 * @pre person != null
	 * @post getAddedPerson().getIdPerson() == person.getIdPerson()
	 * @post getAddedPerson().equals(person)
	 * @invariant isWellFormed()
	 */
	public void addPerson(Person person);
	/**
	 * @pre person != null
	 * @pre bankAccounts.containsKey(person)
	 * @post !bankAccounts.containsKey(person)
	 */
	public void removePerson(Person person);
	/**
	 * @pre oldPerson != null
	 * @pre newPerson != null
	 * @post !bankAccounts.containsKey(oldPerson)
	 * @post bankAccounts.containsKey(newPerson)
	 * @invariant isWellFormed()
	 */
	public void editPerson(Person oldPerson, Person newPerson);
	/**
	 * @pre account != null
	 * @post getPersonAccounts(account.getAccountHolder()).contains(account);
	 * @post getAddedAccount().equals(account)
	 * @invariant isWellFormed()
	 */
	public void addAccount(Account account);
	/**
	 * @pre account != null
	 * @pre getPersonAccounts(account.getAccountHolder()).contains(account);
	 * @post !getPersonAccounts(account.getAccountHolder()).contains(account);
	 */
	public void removeAccount(Account account);
	/**
	 * @pre oldAccount != null
	 * @pre newAccount != null
	 * @pre accounts.contains(oldAccount)
	 * @post !getPersonAccounts(oldAccount.getAccountHolder()).contains(oldAccount)
	 * @post bankAccounts.containsValue(accounts)
	 * @invariant isWellFormed()
	 */
	public void editAccount(Account oldAccount, Account newAccount);
	/**
	 * @pre true
	 * @post bankAccounts.equals(@return)
	 */
	public HashMap<Person, HashSet<Account>> readBankAccounts();
	/**
	 * @pre true
	 * @post @nochange
	 * @invariant isWellFormed()
	 */
	public void writeBankAccounts();
	/**
	 * @pre true
	 * @post bankAccounts.equals(@return) // after 
	 * @invariant isWellFormed()
	 */
	public HashMap<Person, HashSet<Account>> getBankAccounts();
	/**
	 * @pre true
	 * @post @nochange
	 */
	public Person getAddedPerson();
	/**
	 * @pre true
	 * @post @nochange
	 */
	public Account getAddedAccount();
	/**
	 * @pre getCurentPerson() >= idPerson
	 * @post @nochange
	 */
	public Person getPersonById(int idPerson);
	/**
	 * @pre getCurrentIdAccount() >= idAccount
	 * @post @nochange
	 */
	public Account getAccountById(int idAccount, int idPerson);
	/**
	 * @pre bankAccounts.containsKey(person);
	 * @post @nochange
	 */
	public HashSet<Account> getPersonAccounts(Person person);
	/**
	 * @pre true
	 * @post @nochange
	 */
	public List<Person> getPersons();
	public int getCurrentIdPerson();
	public int getCurrentIdAccount();
	/**
	 * @pre true
	 * @post currentIdPerson@pre + 1 = currentIdPerson
	 */
	public void incrementCurrentIdPerson();
	/**
	 * @pre true
	 * @post currentIdAccount@pre + 1 = currentIdAccount
	 */
	public void incrementCurrentIdAcount();
	/**
	 * @pre true
	 * @post @nochange
	 */
	public void readCurrentIds();
	/**
	 * @pre true
	 * @post @nochange
	 * @invariant isWellFormed()
	 */
	public void writeCurrentIds();
	
}
