package bank;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

import bank.validators.*;
import bank.validators.personValidators.*;
import bank.validators.accountValidators.*;

public class Bank implements BankProc, java.io.Serializable{
	
	private static final long serialVersionUID = 5L;
	private int currentIdPerson = 0;
	private int currentIdAccount = 0;
	private HashMap<Person, HashSet<Account>> bankAccounts;
	private Person addedPerson;
	private Account addedAccount;
	private List<Validator<Person>> personValidators;
	private List<Validator<Account>> accountValidators;
	//private HashMap<Person, FileHandler> fileHandlers;
	
	public Bank() {
		this.bankAccounts = this.readBankAccounts();
		this.readCurrentIds();
		assert this.isWellFormed();
		personValidators = new ArrayList<Validator<Person>>();
		personValidators.add(new NameValidator());
		personValidators.add(new phoneNumberValidator());
		personValidators.add(new EmailValidator());
		accountValidators = new ArrayList<Validator<Account>>();
		accountValidators.add(new BalanceValidator());
		accountValidators.add(new TtValidator());
		accountValidators.add(new PersonValidator(this));
		//fileHandlers = new HashMap<Person, FileHandler>();
	}
	
	@Override
	public HashMap<Person, HashSet<Account>> getBankAccounts() {
		assert this.isWellFormed();
		return this.bankAccounts;
	}
	
	@Override
	public Person getAddedPerson() {
		return this.addedPerson;
	}
	
	public void setAddedPerson(Person person) {
		addedPerson = person;
	}
	
	@Override
	public Account getAddedAccount() {
		return this.addedAccount;
	}
	
	@Override
	public Person getPersonById(int idPerson) {
		assert this.getCurrentIdPerson() >= idPerson;
		for (Person person : bankAccounts.keySet()) {
			if (idPerson == person.getIdPerson())
				return person;
		}
		return null;
	}
	
	@Override
	public Account getAccountById(int idAccount, int idPerson) {
		assert getCurrentIdAccount() >= idAccount;
		Person holder = this.getPersonById(idPerson);
		for (Account account : bankAccounts.get(holder)) {
			if (idAccount == account.getAccountId())
				return account;
		}
		return null;
	}
	
	@Override
	public HashSet<Account> getPersonAccounts(Person person) {
		assert bankAccounts.containsKey(person);
		HashSet<Account> accounts = new HashSet<Account>();
		if (bankAccounts.get(person) != null)
			for (Account account : bankAccounts.get(person)) {
				accounts.add(account);
			}
		return accounts;
	}
	
	public List<Person> getPersons() {
		List<Person> persons = new ArrayList<Person>();
		for (Person person : bankAccounts.keySet()) {
			persons.add(person);
		}
		return persons;
	}
	
	@Override
	public void addPerson(Person person) {
		assert person != null : "Persoana nu exista!";
		for (Validator<Person> v : personValidators) {
			v.validate(person);
		}
		this.addedPerson = person;
		this.incrementCurrentIdPerson();
		assert this.getAddedPerson().equals(person);
		assert this.getAddedPerson().getIdPerson() == person.getIdPerson();
		assert this.isWellFormed();
	}
	
	@Override
	public void removePerson(Person person) {
		assert person != null : "Persoana nu exista!";
		assert bankAccounts.containsKey(person);
		if(bankAccounts.remove(person) != null)
			System.out.println(person.toString() + " removed!");
		else
			throw new IllegalArgumentException("Cannot remove person " + person.toString());
		assert !(bankAccounts.containsKey(person)): "Stergere efectuata fara succes!";
	}
	
	@Override
	public void editPerson(Person oldPerson, Person newPerson) {
		assert oldPerson != null : "Persoana nu exista!";
		assert newPerson != null : "Persoana nu exista!";
		HashSet<Account> accounts = this.getPersonAccounts(oldPerson);
		if (bankAccounts.remove(oldPerson) != null) {
			bankAccounts.put(newPerson, accounts);
		}
		else
			throw new IllegalArgumentException("Cannot remove person " + oldPerson.toString());
		assert !bankAccounts.containsKey(oldPerson);
		assert bankAccounts.containsKey(newPerson);
		assert this.isWellFormed();
	}
	
	@Override
	public void addAccount(Account account) {
		assert account != null : "Account-ul nu exista!";
		for (Validator<Account> v : accountValidators) {
			v.validate(account);
		}
		
		if (!bankAccounts.containsKey(account.getAccountHolder())) {
			try {
				FileHandler fh= new FileHandler("person" + account.getAccountHolder().getIdPerson() + "_log.txt", true);
				SimpleFormatter sf = new SimpleFormatter();
				fh.setFormatter(sf);
				//fileHandlers.put(account.getAccountHolder(), fh);
				account.getAccountHolder().LOGGER.addHandler(fh);
				account.getAccountHolder().LOGGER.log(Level.INFO, account.getAccountHolder().toString() + " created " + account.toString() + " " + "| initialBalance: "+ account.getBalance() + "!");
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			//account.getAccountHolder().LOGGER.addHandler(fileHandlers.get(account.getAccountHolder()));
			account.getAccountHolder().LOGGER.log(Level.INFO, account.getAccountHolder().toString() + " created " + account.toString() + " " + "| initialBalance: "+ account.getBalance() + "!");
		}
		this.addedAccount = account;
		HashSet<Account> accounts;
		if (!bankAccounts.containsKey(account.getAccountHolder())) {
			accounts = new HashSet<Account>();
		} else {
			accounts = this.getPersonAccounts(account.getAccountHolder());
		}
		accounts.add(account);
		if (!bankAccounts.containsKey(account.getAccountHolder())) {
			System.out.println("Ar trebui sa ajungi aici doar la primul account");
			bankAccounts.put(account.getAccountHolder(), accounts);
		} else {
			bankAccounts.replace(account.getAccountHolder(), this.getPersonAccounts(account.getAccountHolder()), accounts);
			System.out.println("Adaugat cu replace");
		}
		this.incrementCurrentIdAcount();
		assert this.getPersonAccounts(account.getAccountHolder()).contains(account);
		assert this.getAddedAccount().equals(account);
		assert this.isWellFormed();
	}
	
	@Override
	public void removeAccount(Account account) {
		assert account != null : "Account-ul nu exista!";
		HashSet<Account> accounts = this.getPersonAccounts(account.getAccountHolder());
		assert accounts.contains(account);
		account.dettach();
		if (accounts.remove(account))
			System.out.println(account.toString() + " removed!");
		else
			throw new IllegalArgumentException("Account does not exists!");
		bankAccounts.replace(account.getAccountHolder(), this.getPersonAccounts(account.getAccountHolder()), accounts);
		assert !getPersonAccounts(account.getAccountHolder()).contains(account);
	}
	
	@Override
	public void editAccount(Account oldAccount, Account newAccount) {
		assert oldAccount != null : "Account-ul nu exista!";
		assert newAccount != null : "Account-ul nu exista!";
		HashSet<Account> accounts = this.getPersonAccounts(oldAccount.getAccountHolder());
		accounts.remove(oldAccount);
		accounts.add(newAccount);
		bankAccounts.replace(oldAccount.getAccountHolder(), this.getPersonAccounts(oldAccount.getAccountHolder()), accounts);
		assert !getPersonAccounts(oldAccount.getAccountHolder()).contains(oldAccount);
		assert bankAccounts.containsValue(accounts);
		assert this.isWellFormed();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Person, HashSet<Account>> readBankAccounts() {
		HashMap<Person, HashSet<Account>> savedBankAccounts = new HashMap<Person, HashSet<Account>>();
		try {
			File file = new File("./bankAccountsData.ser");
			if (file.exists() && !file.isDirectory()) {
				FileInputStream fileIn = new FileInputStream("./bankAccountsData.ser");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				savedBankAccounts = (HashMap<Person, HashSet<Account>>) in.readObject();
				in.close();
				fileIn.close();
			}
		} catch(IOException i) {
			i.printStackTrace();
			return savedBankAccounts;
		} catch(ClassNotFoundException c) {
			System.out.println("Class not found");
			c.printStackTrace();
			return savedBankAccounts;
		}
		return savedBankAccounts;
	}

	@Override
	public void writeBankAccounts() {
		assert this.isWellFormed();
		try {
			FileOutputStream fileOut = new FileOutputStream("./bankAccountsData.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(bankAccounts);
			out.close();
			fileOut.close();
			 System.out.println("Serialized data is saved in ./bankAccountsData.ser");
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	@Override
	public void readCurrentIds() {
		try {
			File file = new File("./Ids.ser");
			if (file.exists() && !file.isDirectory()) {
				FileInputStream fileIn = new FileInputStream("./Ids.ser");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				currentIdPerson = in.readInt();
				currentIdAccount = in.readInt();
				in.close();
				fileIn.close();
			}
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	@Override
	public void writeCurrentIds() {
		assert this.isWellFormed();
		try {
			FileOutputStream fileOut = new FileOutputStream("./Ids.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeInt(currentIdPerson);
			out.writeInt(currentIdAccount);
			out.close();
			fileOut.close();
			 System.out.println("Serialized data is saved in ./Ids.ser");
		} catch(IOException i) {
			i.printStackTrace();
		}
	}
	
	@Override
	public int getCurrentIdPerson() {
		return currentIdPerson;
	}
	
	@Override
	public int getCurrentIdAccount() {
		return currentIdAccount;
	}
	
	@Override
	public void incrementCurrentIdPerson() {
		currentIdPerson += 1;
	}
	
	@Override
	public void incrementCurrentIdAcount() {
		currentIdAccount += 1;
	}
	
	public boolean isWellFormed() {
		if (bankAccounts == null) {
			return false;
		} else {
			for (Map.Entry<Person, HashSet<Account>> entry : bankAccounts.entrySet()) {
				if (entry.getKey().getIdPerson() > currentIdPerson)
					return false;
				for (Account account : entry.getValue()) {
					if (account.getAccountId() > currentIdAccount)
						return false;
					if (!account.getAccountHolder().equals(entry.getKey()))
						return false;
					if ((account.getClass() != SavingAccount.class) && (account.getClass() != SpendingAccount.class))
						return false;
					if (account.getBalance() < 0)
						return false;
					if (account.getTransactionCount() < 0)
						return false;
				}
			}
		}
		return true;
	}
}
