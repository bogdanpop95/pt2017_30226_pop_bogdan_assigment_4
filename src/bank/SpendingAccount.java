package bank;

public class SpendingAccount extends Account{
	
	private static final long serialVersionUID = 2L;
	private final int FREE_TRANSACTIONS = 3;
	private final double TRANSACTION_FEE = 1.5;
	
	public SpendingAccount(int id, Person person) {
		super(id, person);
	}
	
	public SpendingAccount(int id, double initialBalance, Person person) {
		super(id, initialBalance, person);
	}
	
	@Override
	public boolean withdraw(double amount) {
		if (this.transactionCount < FREE_TRANSACTIONS) {
			if(super.withdraw(amount)) {
				this.transactionCount++;
				return true;
			}
		} else {
			if(super.withdraw(amount + TRANSACTION_FEE)) {
				this.transactionCount++;
				return true;
			}
		}
		return false;
	}
	
/*	@Override
	public boolean transfer (double amount, Account other) {
		if (this.transactionCount < FREE_TRANSACTIONS) {
			if (super.transfer(amount, other))
				return true;
		} else {
			amount += TRANSACTION_FEE;
			if (super.transfer(amount, other))
				return true;
		}
		return false;
	}*/
	
	public String toString() {
		return "SpendingAccount | id: " + this.getAccountId();
	}
	
	public boolean free() {
		if (transactionCount < FREE_TRANSACTIONS)
			return true;
		return false;
	}
	
	public double getTransactionFee() {
		return TRANSACTION_FEE;
	}
}
