package bank;

import java.util.logging.Level;
import java.util.logging.Logger;
import observerInterfaces.Observer;

public class Person implements java.io.Serializable, Observer{

	protected static final Logger LOGGER = Logger.getLogger(Person.class.getName());
	private static final long serialVersionUID = 4L;
	private int idPerson;
	private String name;
	private String phoneNumber;
	private String address;
	private String email;
	
	public Person(int id, String name, String phoneNumber, String address, String email) {
		super();
		idPerson = id;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.email = email;
	}

	public int getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(int idPerson) {
		this.idPerson = idPerson;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String toString() {
		return "Name: " + this.name + ", id: " + this.idPerson;
	}
	
	// Observer interface method
	@Override
	public void update(Object obj, double amount, int op) {
		if (obj instanceof Account) {
			Account account = (Account) obj;
			if (op == 1) {
				LOGGER.log(Level.INFO, this.toString() + " deposited in " + account.toString() + " " + amount + "!");
			} else if (op == 2) {
				LOGGER.log(Level.INFO, this.toString() + " withdrawed from " + account.toString() + " " + amount + "!");
			}
		}
	}
}
