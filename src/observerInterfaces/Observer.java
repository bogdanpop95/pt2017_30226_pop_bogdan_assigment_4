package observerInterfaces;

public interface Observer {
	
	public void update(Object obj, double amount, int op);
}
