package observerInterfaces;

public interface Subject {
	
	public void attach(Observer o);
	public void dettach();
	public void notifyy(double amount, int op);
}
