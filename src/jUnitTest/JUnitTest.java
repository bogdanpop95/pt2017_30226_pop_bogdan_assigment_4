package jUnitTest;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bank.Account;
import bank.Bank;
import bank.Person;
import bank.SpendingAccount;

public class JUnitTest {
	private static Bank bank;
	private static Person person;
	private static Account account;
	private static int nrTesteExecutate = 0;
	private static int nrTesteCuSucces = 0;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		bank = new Bank();
		person= new Person(bank.getCurrentIdPerson(), "Person Test", "111222333", "Test Address", "address@test");
		account = new SpendingAccount(bank.getCurrentIdAccount(), 10, person);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("S-au executat " + nrTesteExecutate + " teste din care "+ nrTesteCuSucces + " au avut succes!");
	}
	
	@Before
	public void setUp() throws Exception {
		nrTesteExecutate++;
	}
	
	@Test
	public void testAddPerson() {
		Person waitedPerson1, waitedPerson2;
		bank.addPerson(person);
		bank.addAccount(account);
		waitedPerson1 = bank.getAddedPerson();
		waitedPerson2 = bank.getPersonById(person.getIdPerson());
		assertEquals(person, waitedPerson1);
		assertEquals(person, waitedPerson2);
		nrTesteCuSucces++;
	}
	
	@Test
	public void testAddAccount() {
		Account waitedAccount1, waitedAccount2;
		bank.setAddedPerson(person);
		bank.addAccount(account);
		waitedAccount1 = bank.getAddedAccount();
		waitedAccount2 = bank.getAccountById(account.getAccountId(), person.getIdPerson());
		assertEquals(account, waitedAccount1);
		assertEquals(account, waitedAccount2);
		nrTesteCuSucces++;
	}
	
	@Test
	public void testRemoveAccount() {
		bank.removeAccount(account);
		assertTrue(!bank.getPersonAccounts(account.getAccountHolder()).contains(account));
		nrTesteCuSucces++;
	}
	
	@Test
	public void testRemovePerson() {
		bank.removePerson(person);
		assertTrue(!bank.getBankAccounts().containsKey(person));
		nrTesteCuSucces++;
	}
	
}
